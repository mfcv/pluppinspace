
 var initGame = function() {
        var width = $(window).width() - 17,
            height = $(window).height() - 40;
            var game = new Phaser.Game(width, height, Phaser.AUTO, 'phaser_game', { preload: preload, create: create, update: update  }),

            emitters = {
            bass:null,
            chords:null
            },
            textUpdates,
            bombCount = {red:0,blue:0,yellow:0,pink:0,green:0},
            redBombs, 
            greenBombs,
            blueBombs, 
            yellowBombs,
            klangUpdateInterval = 2,
            bombColors = ['rgba(219, 60, 64, 0.8)', 'rgba(94, 255, 181, 0.8)', 'rgba(77, 153, 231, 0.8)', 'rgba(227, 255, 95, 0.8)', 'rgba(230, 96, 254, 0.8)'],
            otherPlayerInstrument = '',
            playerWidth = 30,
            playerHeight = playerWidth,
            lagHi =  0.14,
            lagLo = lagHi/20,
            sloMoLag = 0.0242,
            LAG = lagHi,
            gameInfo = {},
            bassVolIsZero = false, 
            chordsVolIsZero = false,
            MAX_NUMBER_BOMBS = 7, 
            tempo = 121,
            TAILLENGTH = 20,
            TAILINCREASERATIO = 0.10,
            oldCounter = 0,
            changeCounter = 0,
            bombTickInterval = 1050,
            startExplode = false,
            explodeCounter = 14,
            explodingBomb,
            exploding = false,
            error = false,
            splash,
            gameStarted = false,
            bombColor = '',
            goAheadMakeNoise = false,
            preloadBar,
            currentBomb= {
              bomb:null,
              alive:false
            },
            totalBombs = {
              bass:0,
              chords:0
            },
            bmd,
            exp,
            collisionCircle,
            slowRatio = 1.0,
            minFreq = Math.log(200),
            maxFreq = Math.log(15000),
            scale = (maxFreq-minFreq),
            currentState = {
              x:0,
              y:0,
              src: '',
              dest: '',
              notes:{
                bass:[],
                chords:[]
              },
              velocity:{
                bass:[],
                chords:[]
              },
              bombAmount:0,
              tailLength:{
                bass:0,
                chords:0
              },
              colors:[],
              chord:0,
              otherPlayerInstrument:''
            },
            targetPos = {
              bass:{
                x:0,
                y:0
              },
              chords:{
                x:0,
                y:0
              } 
            },
            playerObjects = {
              bass : {
                name : 'bass',
                sprite : null,
                bmd : null,
                circleX : width/2,
                circleY : height/2,
                color : 'rgba(0, 255, 0, 0.8)',
                tailColor : '0x00FF00',
                tailGraphics : null,
                tail : [],
                oldX : 0,
                oldY : 0,
                justCollided : false,
                volIsZero : false,
                totalBombs : 0,
                bombsInTailColors : [],
                tailLength : TAILLENGTH,
                actualBombs : 0
              },
              chords : {
                name : 'chords',
                sprite : null,
                bmd : null,
                circleX : width/2,
                circleY : height/2,
                color : 'rgba(255, 0, 0, 0.8)',
                tailColor : '0xFF0000',
                tailGraphics : null,
                tail : [],
                oldX : 0,
                oldY : 0,
                justCollided : false,
                volIsZero : false,
                totalBombs : 0,
                bombsInTailColors : [],
                tailLength : TAILLENGTH,
                actualBombs : 0
              }
            },
            bmds = {
              bass:null,
              chords:null
            },
            playerColors = {
              bass : 'rgba(0, 255, 0, 0.8)',
              chords : 'rgba(255, 0, 0, 0.8)'
            }

        
        function Point(x, y) {
          this.position = {x: x, y: y};
        }

        function preload () {
            game.load.image('trans_dot', 'assets/trans_dot.png');
            game.load.image('particle_bass', 'assets/particle_bass.png');
            game.load.image('particle_chords', 'assets/particle_chords.png');
            game.load.image('bomb_yellow', 'assets/bomb_yellow.png');
            game.load.image('bomb_blue', 'assets/bomb_blue.png');
            game.load.image('bomb_green', 'assets/bomb_green.png');
            game.load.image('bomb_red', 'assets/bomb_red.png');
            game.load.image('bomb_pink', 'assets/bomb_pink.png');
            game.load.image('square_yellow', 'assets/square_yellow.png');
            game.load.image('square_gray', 'assets/square_gray.png');
            game.load.image('splash', 'assets/splash2.png');
        }
        function create () {
            doneLoading();
            initEmitter();
            textUpdates = game.add.text((width/2) - 100, 10, "", {
              font: "12px Arial",
              fill: "#4d99e7",
              align: "center"
            });
            game.physics.startSystem(Phaser.Physics.ARCADE);
            bmd = game.add.bitmapData(width, height);
            bmds.bass = game.add.bitmapData(width, height);
            bmds.chords = game.add.bitmapData(width, height);
            game.add.sprite(0, 0, bmds.bass);
            game.add.sprite(0, 0, bmds.chords);
            splash = game.add.sprite(width/2, height/2, 'splash');

            if(width < 874){
              var scale = width / 874;
              splash.scale.x = scale - (scale * 0.3);
              splash.scale.y = scale - (scale * 0.3);
            }else{
              splash.scale.x = 0.75;
              splash.scale.y = 0.75;
            }
            splash.x = (width - splash.width) / 2;
            splash.y = (height - splash.height) / 2;
            game.input.onDown.add(start, this);
            playerObjects[playerInstrument].bmd = bmds[playerInstrument];
            exp = game.add.sprite(0, 0, bmd);
            redBombs = game.add.group();
            greenBombs = game.add.group();
            blueBombs = game.add.group();
            yellowBombs = game.add.group();
            pinkBombs = game.add.group();
            chordChangers = game.add.group();
            slowDowners = game.add.group();
            Klang.triggerEvent('kick_start');
            Klang.triggerEvent('bass_start');
            Klang.triggerEvent('chords_start');
            playerObjects[playerInstrument].bmd.circle(playerObjects[playerInstrument].circleX, playerObjects[playerInstrument].circleY, playerWidth, playerObjects[playerInstrument].color);
            playerObjects[playerInstrument].sprite = game.add.sprite(width/2, height/2, 'trans_dot');
            setupSprite(playerObjects[playerInstrument].sprite);
            if(gameInfo.bass && playerInstrument != "bass"){
              playerObjects.bass.sprite = game.add.sprite(width/2, height/2, 'trans_dot');
              setupSprite(playerObjects.bass.sprite);
            }
             if(gameInfo.chords && playerInstrument!="chords"){
              playerObjects.chords.sprite = game.add.sprite(width/2, height/2, 'trans_dot');
              setupSprite(playerObjects.chords.sprite);
            }
            game.time.events.repeat(100, 10000, emitPos, this);
            playerObjects.bass.tailGraphics = game.add.graphics(0, 0);
            playerObjects.chords.tailGraphics = game.add.graphics(0, 0);
            // TODO
            // göm muspekaren, finns tydligen ett sätt att göra det med phaser i dev branch
            // http://www.html5gamedevs.com/topic/2773-hide-mouse-cursor/
        }

        function initEmitter(){

          emitter = game.add.emitter(width/2, height/2, 10000);
          emitters.bass = game.add.emitter(0, 0, 10000);
          emitters.chords = game.add.emitter(0, 0, 10000);
          emitter.setAlpha(0.8, 0.0, 10000);
          emitters.bass.setAlpha(0.8, 0.0, 10000);
          emitters.chords.setAlpha(0.8, 0.0, 10000);
          emitter.makeParticles('particle');
          emitters.bass.makeParticles('particle_bass');
          emitters.chords.makeParticles('particle_chords');
          emitter.setAll('body.allowGravity', false);
          emitters.bass.setAll('body.allowGravity', false);
          emitters.chords.setAll('body.allowGravity', false);
          emitter.start(true, 1, 1, 1);
        }

        function update(){
          if(gameStarted){
            instrumentPhysics(playerObjects[playerInstrument].sprite);
            if(playerObjects[playerInstrument].bmd){
              movePlayer(playerObjects[playerInstrument]);
            }
            if(otherPlayerInstrument != ""){
              if(playerObjects[otherPlayerInstrument].bmd){
                movePlayer(playerObjects[otherPlayerInstrument]);
              }
            }
            explode();
            updateKlang();
          }
        }

        function explosionSlowDown(){
          if(!exploding){
            exploding = true;
            LAG = lagLo;
            slowRatio = 0.07;
            Klang.triggerEvent('master_filter_drop')
            setTimeout(function(){              
              Klang.triggerEvent('master_filter_up')
              LAG = lagHi;
              slowRatio = 1.0;
              exploding = false;
            },1000);
          }
        }

        function createBomb(x, y, type, id){
          switch(type){
            case 0:
              var bomb = redBombs.create(x, y, 'bomb_red');
              bombColor = 'rgba(219, 60, 64,'
            break;
            case 1:
              var bomb = greenBombs.create(x, y, 'bomb_green');
              bombColor = 'rgba(94, 255, 181,'
            break;
            case 2:
              var bomb = blueBombs.create(x, y, 'bomb_blue');
              bombColor = 'rgba(77, 153, 231,'
            break;
            case 3:
              var bomb = yellowBombs.create(x, y, 'bomb_yellow');
              bombColor = 'rgba(227, 255, 95,'
            break;
            case 4:
              var bomb = pinkBombs.create(x, y, 'bomb_pink');
              bombColor = 'rgba(230, 96, 254,'
            break;
            case 5:
              var bomb = chordChangers.create(x, -50, 'square_yellow');

            break;
            case 6:
              var bomb = slowDowners.create(x, -50, 'square_gray');

            break;
          }
            bomb.name = id;
            bomb.anchor.setTo(0.5, 0.5);
            game.physics.arcade.enable(bomb);
            currentBomb.bomb=bomb;
            currentBomb.alive=true;
            if(type<5){
              setTimeout(function(){
                if(bomb.alive){
                  bomb.kill();
                  currentBomb.alive=false;
                  explodeBomb(bomb);
                }
              },bombTickInterval);     
            }else{
              var tween = game.add.tween(bomb);
              var plusOrMinus = 1;
              if(x>(width/2)){
                plusOrMinus= -1;
              }
              var val = ((y / height) * plusOrMinus * x) + x;
              tween.to({x:val, y:height*1.1, alpha: 0.1, angle: plusOrMinus * (y/height) * 720}, 5000, Phaser.Easing.Cubic.Out, true, 100, false)
            }      
        }

        function explode(){
            if(startExplode ){
                bmd.clear();
                var alpha = 1 - ((explodeCounter - 15) * 0.015);
                bmd.circle(explodingBomb.x, explodingBomb.y, explodeCounter, bombColor + alpha + ')');
                collisionCircle = new Phaser.Circle(explodingBomb.x, explodingBomb.y, explodeCounter * 2);
                explodeCounter++;
                if(explodeCounter > 100){
                  explodeCounter = 14;
                  startExplode = false;
                  bmd.clear();
               }
            if(Phaser.Circle.intersectsRectangle(collisionCircle, playerObjects[playerInstrument].sprite.body) && !playerObjects[playerInstrument].justCollided)  {
                  var o = playerObjects[playerInstrument];
                  explosionSlowDown();
                  o.tailLength = TAILLENGTH;
                  o.totalBombs = 0;
                  o.justCollided = true;
                  currentState.notes[playerInstrument] = [0];
                  currentState.velocity[playerInstrument] = [64];
                  Klang.triggerEvent(playerInstrument + '_clear_notes')
                  emitSingle(playerInstrument + '_clear_notes')
                  o.actualBombs = 0;
                  Klang.triggerEvent('hat_stop');
                  Klang.triggerEvent('snare_stop');
                  Klang.triggerEvent('fail');
                  setTimeout(function(){
                    o.justCollided = false;
                    o.bombsInTailColors = [];
                  },2000);
            }
          }
        }

        function particlesWhenCollide(bomb,instrument){
          if(bomb && emitters[instrument]){
            emitters[instrument].x = bomb.x;
            emitters[instrument].y = bomb.y;
            emitters[instrument].start(true, 12000, 250, 20);
          }
        }

        function explodeBomb(bomb){
            startExplode = true;
            explodingBomb = bomb;
            Klang.triggerEvent('explosion_start')
        }

        function drawTail(graphics, sprite, color, tail, bmd, isCurrentPlayer, bombAmount, colorArray, tailLength, vol){
            graphics.clear();
            graphics.lineStyle(2, color, 0.35);
            while( tail.length - 1 < tailLength ) {
              tail.push(new Point( sprite.body.x + (sprite.width/2), sprite.body.y + (sprite.height/2)));//+(player.width/2)+(player.height/2)
            }
            // Draw a curve through the tail
            for(var i = 0; i < tail.length; i++ )   {
              p = tail[i] ;
              p2 = tail[i+1]  ;
              if( i == 0 ){
                // This is the first loop, so we need to start by moving into position
                graphics.moveTo( p.position.x, p.position.y);
              } else if(p2){                  
                // Draw a curve between the current and next trail point
                graphics.lineTo( p.position.x, p.position.y)//, p.position.x + ( p2.position.x - p.position.x ) / 2, p.position.y + ( p2.position.y - p.position.y ) / 2 )  ;
              }
              var percent = vol * 10;//avg * 5 * 0.05 * tailRatio * slowRatio; 
              var randX = (Math.random() * percent)-((percent/2) );
              var randY = (Math.random() * percent)-((percent/2) );
              p.position.x += randX;
              p.position.y += randY;
            }
            graphics.endFill();
            var tailBombs = 0;
            var tailSpace = Math.round(tailLength / (bombAmount + 1));
            for( i = tail.length - 4; i > 0; i-- ) {
              p = tail[i];
              if (i % tailSpace == 0)  {
                  bmd.circle(p.position.x, p.position.y, playerWidth/3 - (tailBombs + 1) * 2 > 5 ? playerWidth/3 - (tailBombs + 1) * 2 : 5 , colorArray[tailBombs]);
                  tailBombs++;
              };
            }
            if(tail.length > tailLength) {
              tail.shift();
            } 
      }
        function start(){
          if(!gameStarted){
            gameStarted = true;
            game.add.tween(splash).to( { alpha: 0 }, 2000, Phaser.Easing.Linear.None, true, 0, 0, false);
            Klang.initIOS();
            emitSingle('goAheadMakeNoise',1);
          }

        }

        function collectBomb (player, bomb) {
            bomb.kill(); 
            gotBomb(bomb)
            particlesWhenCollide(bomb,playerInstrument);
            startHat(bomb);
        }

        function startHat(bomb){
          var o = playerObjects[playerInstrument];
            if(getTypeFromBomb(bomb) < 4){
              if(o.actualBombs >= 3){
                if(o.actualBombs == 3){
                  // console.log("start hat")
                  Klang.triggerEvent('hat_change', 0);
                }else if(o.actualBombs % 3 == 0){
                  // console.log("change hat")
                  Klang.triggerEvent('hat_change', 1);
                }
              }
              o.actualBombs++;
            }
        }

        function updateKlang(){
            //update the sounds in Klang for connected players
            if(changeCounter==0){
                changeInstrumentValues(playerObjects[playerInstrument], true);
                if(otherPlayerInstrument != ""){
                  changeInstrumentValues(playerObjects[otherPlayerInstrument], goAheadMakeNoise);
                }
            }
            changeCounter++;
            if(changeCounter > klangUpdateInterval) changeCounter = 0;
        }

        function changeInstrumentValues(obj, goAhead){
          if(goAhead){
            if(obj.sprite){
              var diffX = Math.abs(parseInt(obj.sprite.body.x) - parseInt(obj.oldX));
              var diffY = Math.abs(parseInt(obj.sprite.body.y) - parseInt(obj.oldY));
            }
            if(obj.sprite && diffX > 1 && diffY > 1){
                var position =  (obj.sprite.body.y/height);
                var val = Math.exp(minFreq + scale * position);
                Klang.triggerEvent(obj.name + '_frequency', val);
                var dec = ((height + 600 - obj.sprite.y)/height) * 0.060;
                Klang.triggerEvent(obj.name + '_decay', dec);
                Klang.triggerEvent(obj.name + '_delay', (obj.sprite.body.x / width) - 0.1) ;
                Klang.triggerEvent(obj.name + '_bus_pan', ((obj.sprite.body.x / width) *2 ) - 1);
                if(obj.volIsZero){
                  Klang.triggerEvent(obj.name + '_bus_volume',1);
                  obj.volIsZero = false;
                }
            }else if(obj.sprite && !obj.volIsZero){
              Klang.triggerEvent(obj.name + '_bus_volume',0);
              obj.volIsZero=true;
            }
          }
        }

        function movePlayer(o){
          if(o.sprite){
              if(o.name == playerInstrument){
                o.sprite.body.x += (game.input.x - o.sprite.body.x) * LAG;
                o.sprite.body.y += (game.input.y - o.sprite.body.y) * LAG;
              }else if(o.name == otherPlayerInstrument){
                o.sprite.body.x += (targetPos[o.name].x - o.sprite.body.x) * LAG;
                o.sprite.body.y += (targetPos[o.name].y - o.sprite.body.y) * LAG;
              }
              if(o.name == playerInstrument || o.name == otherPlayerInstrument){
                o.circleX = o.sprite.body.x;
                o.circleY = o.sprite.body.y;
                try{
                  o.bmd.clear();
                }catch(err){
                  if(!error){
                    console.log("An error occured, please reload.")
                    error = true;
                  }
                }
                drawTail(o.tailGraphics, o.sprite, o.tailColor, o.tail, o.bmd, true, o.totalBombs, o.bombsInTailColors, o.tailLength, 0);
                try{
                o.bmd.circle(o.circleX + playerWidth/2,
                  o.circleY + playerWidth/2, playerWidth/2, o.color);
               }catch(err){
                  if(!error){
                    console.log("An error occured, please reload.")
                    error = true;
                  }
                }
                if(oldCounter == 10){
                  o.oldX = Math.round(o.sprite.body.x);
                  o.oldY = Math.round(o.sprite.body.y);
                }
                if(oldCounter>10) oldCounter = 0;
                oldCounter++;
              }
            }
        }

        // emit mouse pos if changed
        function emitPos(){
          if((Math.round(game.input.x) != playerObjects[playerInstrument].oldX || Math.round(game.input.y) != playerObjects[playerInstrument].oldY) && gameStarted){
              server.emit('pp', {
               p:playerInstrument,
               x:game.input.x/width,
               y:game.input.y/height
              });
          }
        }

         //events from server
        server.on('pp', function(info){
          targetPos[info.p].x = info.x * width;
          targetPos[info.p].y = info.y * height;
        })

        server.on('player_joined', function(info){
          setTimeout(function(){
            textUpdates.setText("Another player has joined the game.");
            // textUpdates.setText("A player" + fromCountry + " has joined the game.");
            setTimeout(function(){
            textUpdates.setText("");
            }, 5000);
            var newPlayer = playerObjects[info.curPlayer];
            newPlayer.bmd = bmds[info.curPlayer];
            var currentPlayer = playerObjects[playerInstrument];
            newPlayer.sprite = game.add.sprite(targetPos[info.curPlayer].x, targetPos[info.curPlayer].y, 'trans_dot');
            setupSprite(newPlayer.sprite);
            otherPlayerInstrument = info.curPlayer;
            currentState.otherPlayerInstrument = playerInstrument;
            currentState.chord = Klang.getUtil().vars.currentChord;
            currentState.colors = playerObjects[playerInstrument].bombsInTailColors;
            currentState.totalBombs = currentPlayer.totalBombs;
            currentState.tailLength = currentPlayer.tailLength;
            currentState.dest = info.curPlayer;
            currentState.src = playerInstrument;
            currentState.x = game.input.x;
            currentState.y = game.input.y;
            server.emit('current_state', currentState);
          },1500);
        })

        server.on('current_state', function(info){
          // console.log("state received",info)
          var o = playerObjects[info.src];
          setTimeout(function() {
            for (var i = 0; i < info.notes[info.src].length; i++) {
              if(info.notes[info.src][i] != 0)
                Klang.triggerEvent(info.src + '_add_note', info.notes[info.src][i], info.velocity[info.src][i])
            }
            o.tailLength = info.tailLength;
            o.bombsInTailColors = info.colors;
            o.totalBombs = info.totalBombs;
            o.bmd = bmds[info.src];
            if(o.sprite){
              o.sprite.body.x = info.x;
              o.sprite.body.y = info.y;
            }
            Klang.triggerEvent('change_chord', info.chord);
            otherPlayerInstrument = info.otherPlayerInstrument;
          },2500);
          goAheadMakeNoise = true;
        });
      
        server.on("remove_instrument", function(info){
            textUpdates.setText("The other player has left the game.");
            goAheadMakeNoise = false;
            setTimeout(function(){
              textUpdates.setText("");
            }, 5000);
            var o = playerObjects[info.instrument];
            o.sprite.destroy();
            if(o.bmd) o.bmd.clear();
            o.sprite = null;
            Klang.triggerEvent(info.instrument + '_clear_notes')
            Klang.triggerEvent(info.instrument + '_bus_volume',0)
            o.totalBombs = 0;
            o.bombsInTailColors = [];
        });

        server.on('curPlayer', function(info){
          gameInfo = info;
      })

        server.on('randomBomb', function(info){
          if(gameStarted){
            var type = info.type;
            if(type<5){
              Klang.triggerEvent('play_bomb_swoosh',(info.x * 2) - 1);
            }
            setTimeout(function(){
              var x = (width * 0.1) + (info.x * width * 0.8); 
              var y = (height * 0.1) + (info.y * height * 0.8); 
              createBomb(x, y, type, info.id);
            },300)
          }
        })

        server.on('delete_bomb', function(type){
          if(currentBomb.alive){
            var type = getTypeFromBomb(currentBomb.bomb);
            var x = currentBomb.bomb.x/width;
            Klang.triggerEvent('enemy_plupp', 1 + (type * 0.3), (x * 2) - 1)
            particlesWhenCollide(currentBomb.bomb, otherPlayerInstrument);
            currentBomb.bomb.kill();
          }
          startExplode = false;
          bmd.clear();
        })        

        server.on('singleEvent', function(origin){
          var bass = playerObjects['bass'];
          var chords = playerObjects['chords'];
          switch(origin.action){
            case 'goAheadMakeNoise':
            goAheadMakeNoise = true;
            break;
            case 'bass_add_note':             
              Klang.triggerEvent('bass_add_note',origin.status,origin.extra);
              break;
            case 'change_chord':        
              Klang.triggerEvent('change_chord',origin.status);
            break;
            case 'bass_clear_notes':
              Klang.triggerEvent('bass_clear_notes');
              bass.tailLength = TAILLENGTH;
              bass.totalBombs = 0;
              setTimeout(function(){
                bass.bombsInTailColors = [];
              },1000);
              break;

            case 'addBombToTail':
              playerObjects[otherPlayerInstrument].bombsInTailColors.push(origin.status);
              playerObjects[otherPlayerInstrument].totalBombs++;
              playerObjects[otherPlayerInstrument].tailLength += playerObjects[otherPlayerInstrument].tailLength * TAILINCREASERATIO;
            break;
            case 'chords_add_note':
              Klang.triggerEvent('chords_add_note', origin.status,origin.extra)
              break;
            case 'chords_clear_notes':
              Klang.triggerEvent('chords_clear_notes')
              chords.tailLength = TAILLENGTH;
              chords.totalBombs = 0;
              setTimeout(function(){
                chords.bombsInTailColors = [];
              },1000);
              break;
            }
        })

      server.on('bombConfirmed', function(info){
        var o = playerObjects[playerInstrument];
          if(o.totalBombs <= MAX_NUMBER_BOMBS && info.type<5){
            var vel = info.type * 25;
            // if(vel == 0) vel = 35;
            var randCeil = Klang.getUtil().vars.chords[Klang.getUtil().vars.currentChord].length - 1; 
            var rand = Math.floor(Math.random() * randCeil);
            o.bombsInTailColors.push(bombColors[info.type]);
            emitSingle('addBombToTail',bombColors[info.type]);
            Klang.triggerEvent(playerInstrument + '_add_note', rand, vel);
            emitSingle(playerInstrument + '_add_note',rand,vel);
            currentState.notes[playerInstrument].push(rand);
            currentState.velocity[playerInstrument].push(vel);
            o.totalBombs++;
            o.tailLength += o.tailLength * TAILINCREASERATIO;
            }else if(info.type > 4){
              switch (info.type){
                case 5:
                  changeChord();
                break
                case 6:
                  Klang.triggerEvent('slowmotion_start');
                  setTimeout(slowMotionStart,70)
                break;
              }
            }
      })

        function setupSprite(sprite){
          if(sprite){
            sprite.renderable = false;
            sprite.height = playerHeight;
            sprite.width = playerWidth;
            game.physics.enable(sprite, Phaser.Physics.ARCADE);
            sprite.anchor.setTo(0.5, 0.5);
          }
        }

        function instrumentPhysics(sprite){
              game.physics.arcade.overlap(sprite, redBombs, collectBomb, null, this);
              game.physics.arcade.overlap(sprite, greenBombs, collectBomb, null, this);
              game.physics.arcade.overlap(sprite, blueBombs, collectBomb, null, this);
              game.physics.arcade.overlap(sprite, yellowBombs, collectBomb, null, this);
              game.physics.arcade.overlap(sprite, pinkBombs, collectBomb, null, this);
              game.physics.arcade.overlap(sprite, chordChangers, collectBomb, null, this);
              game.physics.arcade.overlap(sprite, slowDowners, collectBomb, null, this);
        }

        function emitSingle(action, status, extra){
              server.emit('singleEvent', {
              instrument : playerInstrument,
              action : action,
              status: status,
              extra:extra
              });
        }

        function gotBomb(bomb){
          var type = getTypeFromBomb(bomb);
          var x = bomb.x / width;
          var id = bomb.name;
          server.emit('got_bomb', {
            id:id,
            type:type
          });
          if(type==6) type = 0.5
          Klang.triggerEvent('plupp_play',1 + (type*0.3), (x * 2) - 1);
        } 

        function getTypeFromBomb(bomb){
          if(bomb){
            switch(bomb.key){
                  case 'bomb_red':
                    return 0;
                  case 'bomb_blue':
                    return 1;         
                  case 'bomb_green':
                    return 2;
                  case 'bomb_yellow':
                    return 3;
                  case 'bomb_pink':
                    return 4;
                  case 'square_yellow':
                    return 5;
                  case 'square_gray':
                    return 6;  
                }
            }else{
              return 0;
            }
        }

        function changeChord(){
          // hitta aktuellt ackord i klang
          var chord = Klang.getUtil().vars.currentChord;
          chord++;
          if(chord==Klang.getUtil().vars.chords.length){
            chord=0;
          }
          Klang.triggerEvent('change_chord', chord)
          emitSingle('change_chord', chord)
        }

      function slowMotionStart() {

        tempo = Klang.getCoreInstance().findInstance('GMPb').bpm;
        if (tempo != 121) {
            tempo += 0.5;
            Klang.triggerEvent('change_tempo', tempo)
            sloMoLag += 0.000807;
            lagHi =  sloMoLag;
            lagLo = lagHi/20;
            LAG = lagHi;
            setTimeout(slowMotionStart, 70);
        }else{
          sloMoLag = 0.0242
        }
      }
  };