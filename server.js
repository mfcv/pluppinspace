
var http = require('http');
var express = require('express');
var app = express();

app.use(express.static('public'));

app.set('port', process.env.OPENSHIFT_NODEJS_PORT || 8080);
app.set('ip', process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1');

var httpServer =http.createServer(app).listen(app.get('port'), app.get('ip'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});


var playerCount = 0;
var game;
var games = {};
var playersInfo = {};
var infoCount = 0;
var gameId = "";

function generateGameId() {
    var id = "";
    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    for(var ix = 0; ix < 4; ix++) {
        id += chars.charAt(Math.floor(Math.random() * chars.length));
    }

    if (games[id]) {
        return generateGameId();
    }else {
        return id;
    }
}
function getNewType(client){
    var type = Math.floor(Math.random() * 7);

    while(client.game.oldType == type || client.game.oldOldType == type){
        type = Math.floor(Math.random() * 7);
    }
    client.game.oldOldType = client.game.oldType;
    client.game.oldType = type;
    return type;
}

function emitRandom(client){
    var type = getNewType(client);
        info = {
            type: type,
            x: Math.random(),
            y: Math.random(),
            id: ++ client.game.bombId 
        }
        client.game.currentBomb.id = client.game.bombId;
        client.game.currentBomb.type = type;
        client.game.currentBomb.taken = false;
        emitRandomToAllPlayersInGame(client, 'randomBomb', info);    
}
function randomLoop(client, first){
    if(client.game){
        if(client.game.bass || client.game.chords){
            if(first){
                rand = 2000;
            }else{
                // var rand = (1000 + (Math.random() * 3000));
                var rand = 3500;

            }
            setTimeout(function(){
                emitRandom(client);
                randomLoop(client,false);
            }, rand);
        }
    }
}

var io = require("socket.io").listen(httpServer);

io.sockets.on("connection", function(client) {


    client.on('got_bomb', function(info){

        if(client.game){
            if (client.game.currentBomb.id == info.id && client.game.currentBomb.taken == false) {
                client.game.currentBomb.taken = true;
                info.type = client.game.currentBomb.type;
                client.emit('bombConfirmed',info);
                emitToAllPlayersInGame(client, 'delete_bomb', info.id);
            }else if(client.game.currentBomb.id == info.id && client.game.currentBomb.taken == true){
                emitToAllPlayersInGame(client, 'delete_bomb', client.game.currentBomb.type);
            }
        }
    })    

    client.on('disconnect', function(){
        var game = client.game;

        var removeInstrument = "";
        if(game){

        if(this == game.bass){
            removeInstrument = "bass";
            game.bass = null;
            if(!game.chords){
                delete games[game.gameId];
            }
        }
        else if(this == game.chords){
            removeInstrument = "chords";
            game.chords = null;
            if(!game.bass){
                delete games[game.gameId];
            }            
        }

        emitToAllPlayersInGame(this, "remove_instrument", {instrument:removeInstrument});
        client = null;
        playerCount--;
    }


    })

//när en sida laddas skapas ett nytt spel eller så joinar man ett spel, beroende på vad som finns ledigt
    client.on('createOrJoin', function(){
        var info = {};
        var instruments = {};
        var game;
        // var gameId;
        var gameCreated = false;
        var emptyGameFound = false;
        info.bass = false;
        info.chords = false;

        //joina tom plats
        for (var g in games) {
            var game = games[g];
            if(!game.bass || !game.chords){
                emptyGameFound = true;
                if(!game.bass){
                    game.bass = client;
                }
                else if(!game.chords){
                    game.chords = client;
                }
                client.game = game;

                if(game.bass){
                    info.bass = true;
                }
                if(game.chords){
                    info.chords = true;
                }
                break;
            }
        }
        if(!emptyGameFound){
            console.log("creating game..")

            gameId = generateGameId();
            games[gameId] = {
                gameId:gameId,
                bombId:0,
                bass: client,
                chords: null,
                oldType: -1,
                oldOldType: -2,
                currentBomb: {
                    id:0,
                    taken: false,
                    type:-1
                }
            };
            info.bass = true;
            game = games[gameId];
            client.game = game;
            randomLoop(client,true);
        }

        info.curPlayer = getClientsInstrument(client, client.game);
        emitToAllPlayersInGame(client, 'player_joined', info);
        info.gameId = client.game.gameId;
        client.emit('curPlayer', info);
        playerCount++;

    });

    client.on("info", function(info){
        switch (info.instrument){

            case 'bass':   
                playersInfo.bassInfo = info; 
                break;
            case 'chords':
                playersInfo.chordsInfo = info;
                break;
 
        }
    });

    client.on('current_state', function(current_state){
      var game = client.game;
      switch (current_state.dest){

        case 'bass':
            if(game.bass != null){   
                game.bass.emit('current_state',current_state);
                break;
            }
        case 'chords':
            if(game.chords != null){
                game.chords.emit('current_state',current_state);
                break;
            }
        }
    });


    client.on('singleEvent', function(info){
        emitToAllPlayersInGame(client, 'singleEvent', info);
    });

    //Player Position
    client.on('pp', function(info){
        emitToAllPlayersInGame(client, 'pp', info);
    })


});

// hjälpmetoder
// kolla vilket instrument aktuell klient har

function getClientsInstrument(client, game){

    if(client == game.bass){
        return "bass";
    }
    if(client == game.chords){
        return "chords";
    }

    return false;
}

// skicka event till alla spelare i rummet

function emitToAllPlayersInGame(client, eventString, info){

    var game = client.game;
    if(game){
        if(game.bass && getClientsInstrument(client, client.game) != "bass"){
            game.bass.emit(eventString, info);
        }
        if(game.chords && getClientsInstrument(client, client.game) != "chords"){
            game.chords.emit(eventString, info);
        }
    }
}

function emitRandomToAllPlayersInGame(client, eventString, info){
    var game = client.game;
    if(game){
        if(game.bass){
            game.bass.emit(eventString, info);
        }
        if(game.chords){
            game.chords.emit(eventString, info);
        }
    }

}

